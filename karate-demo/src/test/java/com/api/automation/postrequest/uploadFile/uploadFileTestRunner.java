package com.api.automation.postrequest.uploadFile;

import com.intuit.karate.junit5.Karate;
import com.intuit.karate.junit5.Karate.Test;

public class uploadFileTestRunner {
	
	@Test
	public Karate runTest() {
		return Karate.run("uploadFile").relativeTo(getClass());
	}

}