Feature: Validation of Post method. This test cases includes posting the job and validates that it is posted successfully .

  Background: base url for all scenarios
    Given url 'http://localhost:9897'

  Scenario: Validation of POST method using JSON format
    Given path '/normal/webapi/add'
    * def getJobId = function() {return Math.floor((100) * Math.random());}
    * def JobId = getJobId()
    * def reqBody = read("data/jsonData.json")
    And request reqBody
    And headers {Accept: 'application/json', Content-Type: 'application/json'}
	  When method post
    Then status 201
    And print response
    And match response.jobTitle == "Dog Trainer"

    
  Scenario: Validation of POST method using xml format
    Given path '/normal/webapi/add'
    * def reqBody = read("data/xmlData.xml")
    And request reqBody
    And headers {Accept: 'application/json', Content-Type: 'application/xml'}
    When method post
    Then status 201
    And print "PRINTED RESPONSE==> ",  response
    And match response.jobId == 109