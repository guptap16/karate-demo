Feature: upload file using karate framework

  Background: base url for all scenarios
    Given url 'http://localhost:9897'

  Scenario: to upload file in the test application
    Given path '/normal/webapi/upload'
    #location of file, name of the file, content-type header value
    And multipart file file = { read: 'Test.txt', filename: 'Test.txt', Content-type: 'multipart/form-data'} 
		When method post
		Then status 200
		And print response