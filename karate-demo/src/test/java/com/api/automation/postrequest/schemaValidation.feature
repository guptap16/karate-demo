Feature: Validation of Post method. This test cases includes posting the job and validates that it is posted successfully .

  Background: base url for all scenarios
    Given url 'http://localhost:9897'

  Scenario: Validation of POST method using JSON format
    Given path 'normal/webapi/add'
    * def reqBody = read("data/jsonData.json")
    And request reqBody
    And headers {Accept: 'application/json', Content-Type: 'application/json'}
    When method post
    Then status 201
    And print response
    # schema validation using fuzzy matcher
    And match response ==
      """
      {
      "jobId": '#number',
      "jobTitle": '#string',
      "jobDescription": '#string',
      "experience": '#[] #string',
      "project": '#[] #object'
      }    
      """

  Scenario: Validation of schema using fuzzy matcher
    Given path 'normal/webapi/all'
    And header Accept = 'application/json'
    When method get
    Then status 200
    And print response
    # Embedded expression
    * def projectSchema = {"projectName": '#string',"technology": '#[] #string' }
    * def ItemBodySchema = { "jobId": '#number',"jobTitle": '#string',"jobDescription": '#string',"experience": '#[] #string',"project": '#[] ##(projectSchema)'}
    # schema validation using fuzzy matcher
    And match response ==
      """
      '#[] ##(ItemBodySchema)' 

      """
