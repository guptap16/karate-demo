package com.api.automation.getrequest;

import com.intuit.karate.junit5.Karate;
import com.intuit.karate.junit5.Karate.Test;

public class getTestRunner {
	
	@Test
	public Karate runTest() {
		return Karate.run("getRequest", "getResponse", "validateJson", "readDataFrmFile", "jsonPathExpression").relativeTo(getClass());
	}

}