Feature: to test the get end point of application

  Background: Set up Base path
    Given url 'http://localhost:9897'
    And print '*****************BASE PATH******************'

  Scenario: to get all the data from application in JSON format
    And path '/normal/webapi/all'
    And header Accept = 'application/xml'
    When method get #send the get request
    Then status 200 # the status code response should be 200

  Scenario: separate base url and context url
    And path '/normal/webapi/all'
    And header Accept = 'application/json'
    When method get
    Then status 200
