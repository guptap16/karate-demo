Feature: To get response and print

  Background: base url for all scenarios
    Given url 'http://localhost:9897'

  Scenario: To get the response in json
    Given path '/normal/webapi/all'
    And header Accept = 'application/json'
    When method get
    Then status 200
    And match response.[0].jobId == 1
    And match response.[0].experience[1] == 'Apple'
    And match response.[0].project[0].projectName == 'Movie App'
    And match response.[0].project[0].technology[2] == 'Gradle'
    # validate the size of Json array
    And match response.[0].project[0].technology == '#[3]'
    # Validate using wild card
    And match response.[0].project[0].technology[*] == ['Kotlin','SQL Lite','Gradle']
		# contains property
		And match response.[0].project[0].technology[*] contains 'Gradle'