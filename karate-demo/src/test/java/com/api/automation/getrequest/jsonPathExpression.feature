Feature: to test the get end point of application

  Background: Set up Base path
    Given url 'http://localhost:9897'

  Scenario: separate base url and context url
    Given path '/normal/webapi/all'
    When method get
    Then status 200
    # Json path expression, documentation can be found here https://github.com/json-path/JsonPath
    * def jobTitle = karate.jsonPath(response,"$[?(@.jobId == 1)].jobTitle")
    And print "Job Title ==>", jobTitle
