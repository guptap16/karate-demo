Feature: To get response and print

  Background: base url for all scenarios
    Given url 'http://localhost:9897'

  Scenario: To get the response in json and vaildate data from readData.json
    Given path '/normal/webapi/all'
    And header Accept = 'application/json'
    When method get
    Then status 200
   #create a variable to store the data from external file
   * def actualResponse =  read("../readData.json")
   And match response == actualResponse
   And print "Data from variable ==>", actualResponse
      

  Scenario: To get response in xml
    Given path '/normal/webapi/all'
    And header Accept = 'application/xml'
    When method get
    Then status 200
    And print response
    And match response !=
      """
      <List>
      <item>
      <jobId>11</jobId>
      <jobTitle>Software Engg</jobTitle>
      <jobDescription>To develop andriod application</jobDescription>
      <experience>
        <experience>Google</experience>
        <experience>Apple</experience>
        <experience>Mobile Iron</experience>
      </experience>
      <project>
        <project>
          <projectName>Movie App</projectName>
          <technology>
            <technology>Kotlin</technology>
            <technology>SQL Lite</technology>
            <technology>Gradle</technology>
          </technology>
        </project>
      </project>
      </item>
      </List>
      """

  Scenario: Validation of nested object
    Given path '/normal/webapi/all'
    And header Accept = 'application/json'
    When method get
    Then status 200
    And match response contains deep {"jobTitle": "Software Engg"}
    And match response contains deep {"project": [{"projectName": "Movie App",}]}
